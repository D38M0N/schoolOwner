package scanner.controller;

import java.util.Scanner;

public class ScannerController implements scanner.view.ScannerMVC.Controller {

    @Override
    public int nextInt() {
        return new Scanner(System.in).nextInt();
    }

    @Override
    public int pickOption() {
        return new Scanner(System.in).nextInt();
    }

    @Override
    public double nextDouble() {
        return new Scanner(System.in).nextDouble();
    }

    @Override
    public String nextLine() {
        Scanner scanner = new Scanner(System.in);
        return scanner.next() + scanner.nextLine(); //scanner.next() +
    }
}
