import school.view.SchoolView;

public class Main {
    public static void main(String[] args) {
        new SchoolView().run();
    }
}
