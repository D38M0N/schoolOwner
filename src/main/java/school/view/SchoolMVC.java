package school.view;

import menu.view.MenuMVC;

public interface SchoolMVC {

    interface View {
        void run();
        void printAskForNewImprovementGrade();
        void printAskForGradeId();
        void printAskForSubjectId();
        void printAllSubjects();

        void printWelcomeMessage();

        void printAllGroups();

        void printAskForNewGroupMessage();

        void printAskForNewStudentsName();

        void printAskForNewStudentsSurname();

        void printAskForNewGrade();

        void printAllStudents();

        void printAskForGroupId();

        void printAskForStudentId();

        void printAskNameSubject();
        void printEndProgramMessage();

        void printSubjectForStudent(int studentID);

        void printGradeForSubjectWithStudentToImprovement(int subjectWithStudentID);

        void printAskForSubjectForStudentId();

        void printStudentInGroup();

        void printStudentInGroup(int groupID);
    }

    interface Controller {
        void attach(SchoolMVC.View schoolView, MenuMVC.View menuView);
    }
}
