package school.view;

import console.controller.ConsoleController;
import console.view.ConsoleMVC;
import menu.view.MenuView;
import school.controller.SchoolController;
import school.model.database.Database;

public class SchoolView extends MenuView implements SchoolMVC.View {
    SchoolMVC.Controller schoolController;
    ConsoleMVC.Controller consoleController;

    public SchoolView() {
        this.schoolController = new SchoolController();
        this.consoleController = new ConsoleController();
    }

    public void run() {
        schoolController.attach(this, this);
    }

    public void printWelcomeMessage() {
        consoleController.printLine("Welcome in the school manager program");
    }
    public void printAskNameSubject(){
        consoleController.printLine("Enter the name of the newly created subject");
    }

    public void printAllSubjects() {
        consoleController.printLine("Subject:");
        Database.getInstance().getSubjectDAO().getSubjects()
                .forEach(subject -> consoleController.printLine(subject.toString()));
    }

    public void printAllGroups() {
        consoleController.printLine("GROUPS:");
        Database.getInstance().getGroupDAO().getGroups().stream()
                .forEach(group -> consoleController.printLine(group.toString()));
    }

    public void printAllStudents() {
        consoleController.printLine("STUDENTS:");
        Database.getInstance().getStudentDAO().getStudents().stream()
                .forEach(student -> consoleController.printLine(student.toString()));
    }

    public void printAskForNewGroupMessage() {
        consoleController.printLine("Enter the name of the newly created students group");
    }

    public void printAskForNewStudentsName() {
        consoleController.printLine("Enter the name of the newly created student");
    }
    public void printAskForNewImprovementGrade() {
        consoleController.printLine("Enter improvement grade");
    }

    public void printAskForNewStudentsSurname() {
        consoleController.printLine("Enter the surname of the newly created student");
    }
    public void printAskForGradeId() {
        consoleController.printLine("Please input grade id: ");
    }
    public void printAskForStudentId() {
        consoleController.printLine("Please input student id: ");
    }
    public void printAskForSubjectId() {
        consoleController.printLine("Please input subject id: ");
    }


    @Override
    public void printAskForNewGrade() {
        consoleController.printLine("Please input new grade: ");
    }

    public void printAskForSubjectForStudentId() {
        consoleController.printLine("Please input subject id: ");
    }

    @Override
    public void printSubjectForStudent(int studentID) {

        consoleController.printLine("Available subject for student ");
        Database.getInstance().getSubjectDAO().getSubjectsForStudent(studentID).stream()
                .forEach(subjectToStudent -> System.out.println(subjectToStudent.toString()));
    }

    @Override
    public void printGradeForSubjectWithStudentToImprovement(int subjectWithStudentID) {
        Database.getInstance().getGradeDAO().getGrates().stream()
                .filter(grade -> grade.getSubjectWithStudentId() == subjectWithStudentID)
                .filter(grade -> grade.getImprovedGrade()<0)
                .forEach(grade -> System.out.println(grade.toString()));
    }

    @Override
    public void printStudentInGroup() {
        Database.getInstance().getGroupDAO().getDatabaseStudentToGroups().stream()
                .forEach(studentToGroup -> System.out.println(studentToGroup.toString()));
    }

    @Override
    public void printStudentInGroup(int groupID) {
        Database.getInstance().getGroupDAO().getDatabaseStudentToGroups().stream()
                .filter(studentToGroup -> studentToGroup.getGroupId() == groupID)
                .forEach(studentToGroup -> System.out.println(studentToGroup.toString()));
    }

    public void printAskForGroupId() {
        consoleController.printLine("Please input group id: ");
    }
}
