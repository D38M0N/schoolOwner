package school.controller;

import menu.controller.MenuController;
import menu.model.MenuOptions;
import menu.view.MenuMVC;
import scanner.controller.ScannerController;
import scanner.view.ScannerMVC;
import school.view.SchoolMVC;

public class SchoolController implements SchoolMVC.Controller {
    public static final int END_PROGRAM = 0;

    private SchoolMVC.View schoolView;
    private MenuMVC.View menuView;

    private MenuMVC.Controller menuController;
    private ScannerMVC.Controller scannerController;

    public SchoolController() {
        menuController = new MenuController();
        scannerController = new ScannerController();
    }

    @Override
    public void attach(SchoolMVC.View schoolView, MenuMVC.View menuView) {
        this.schoolView = schoolView;
        this.menuView = menuView;

        this.schoolView.printWelcomeMessage();
        menuController.loadMenu();

        chooseNextMenuOption();
    }

    private void chooseNextMenuOption() {
        menuView.printMenuOptions();
        int menuOption = scannerController.pickOption();
        MenuOptions menuOptions = menuController.getMenuOptions();

        if (menuOption != END_PROGRAM) {
            menuOptions.getMenuItem(menuOption).performActions(schoolView);
            chooseNextMenuOption();
        }
        menuOptions.getMenuItem(menuOption).performActions(schoolView);
    }
}
