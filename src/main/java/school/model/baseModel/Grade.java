package school.model.baseModel;

public class Grade {
    private int id;
    private double grade;
    private int subjectWithStudentId;
    private double improvedGrade = -1;



    public int getSubjectWithStudentId() {
        return subjectWithStudentId;
    }

    public void setSubjectWithStudentId(int subjectWithStudentId) {
        this.subjectWithStudentId = subjectWithStudentId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getGrade() {
        return grade;
    }

    public void setGrade(double grade) {
        this.grade = grade;
    }

    public double getImprovedGrade() {
        return improvedGrade;
    }

    public void setImprovedGrade(double improvedGrade) {
        this.improvedGrade = improvedGrade;
    }

    @Override
    public String toString() {
        return "Grade{" +
                "id=" + id +
                ", grade=" + grade +
                ", subjectWithStudentId=" + subjectWithStudentId +
                ", improvedGrade=" + improvedGrade +
                '}';
    }
}
