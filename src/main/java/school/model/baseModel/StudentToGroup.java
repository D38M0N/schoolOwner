package school.model.baseModel;

public class StudentToGroup {
    private int id;
    private int groupId;
    private int studentId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    @Override
    public String toString() {
        return "StudentToGroup{" +
                "id=" + id +
                ", groupId=" + groupId +
                ", studentId=" + studentId +
                '}';
    }
}
