package school.model.baseModel;

public enum MenuOptions {
    CREATE_NEW_CLASS,
    ADD_SUBJECTS_TO_STUDENT,
    ADD_GRADES,
    IMPROVE_GRADE,
    END_SCHOOL_YEAR,
    NO_MOVEMENT;

    public MenuOptions pickOption (int option) {
        switch (option) {
            case 0:
                return CREATE_NEW_CLASS;
            case 1:
                return ADD_SUBJECTS_TO_STUDENT;
            case 2:
                return ADD_GRADES;
            case 3:
                return IMPROVE_GRADE;
            case 4:
                return END_SCHOOL_YEAR;
            default:
                return NO_MOVEMENT;

        }

    }
}
