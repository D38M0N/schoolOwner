package school.model.baseModel;

import java.util.List;

public interface SubjectListener {

    void getSubjects(List<Subject> subjects);

}