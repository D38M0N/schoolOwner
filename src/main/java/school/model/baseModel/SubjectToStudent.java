package school.model.baseModel;

public class SubjectToStudent {

    private int id;
    private int subjectId;
    private int studentId;
    private double average;

    public double getAverage() {
        return average;
    }

    public void setAverage(double average) {
        this.average = average;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(int subjectId) {
        this.subjectId = subjectId;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    @Override
    public String toString() {
        return "SubjectToStudent{" +
                "id=" + id +
                ", subjectId=" + subjectId +
                ", studentId=" + studentId +
                ", average=" + average +
                '}';
    }
}
