package school.model.database;

import school.model.baseModel.Grade;
import school.model.baseModel.SubjectToStudent;

import java.util.List;

public interface GradeDAO {
    void createGradeId(Grade grade);

    void addGrade(double gradeToAdd, SubjectToStudent subjectToStudent);


    void removeGradeStudentOnId(int id);

    List<Grade> getGrates();

    void improveGrade(int gradeId, double improvedGrade);

    void calculateAverage(SubjectToStudent subjectToStudent);
}
