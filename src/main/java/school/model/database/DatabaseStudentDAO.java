package school.model.database;

import school.model.baseModel.Student;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class DatabaseStudentDAO  implements  StudentDAO{
    private List<Student> students = new ArrayList<>();
    public void addStudent(Student student) {
        if (students.isEmpty()) {
            student.setId(1);
        } else {
            student.setId(students.get(students.size() - 1).getId() + 1);
        }
        students.add(student);
    }

    public void removeStudentOnId(int id) {
        students = students.stream()
                .filter(student -> student.getId() != id)
                .collect(Collectors.toList());
    }


    public List<Student> getStudents() {
        return students;
    }
}
