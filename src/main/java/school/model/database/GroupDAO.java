package school.model.database;

import school.model.baseModel.Group;
import school.model.baseModel.StudentToGroup;

import java.util.List;

public interface GroupDAO {
    void createGroupId(Group group);
    void addStudentToGroup(int groupId, int studentId);
    void createGroupToStudentId(StudentToGroup studentToGroup);
    List<Group> getGroups();
    List<StudentToGroup> getDatabaseStudentToGroups();
}
