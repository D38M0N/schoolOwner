package school.model.database;

import school.model.baseModel.Student;

import java.util.List;

public interface StudentDAO {
    void addStudent(Student student);
    void removeStudentOnId(int id);
    List<Student> getStudents();
}
