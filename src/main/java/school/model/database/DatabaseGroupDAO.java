package school.model.database;

import school.model.baseModel.Group;
import school.model.baseModel.StudentToGroup;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class DatabaseGroupDAO implements GroupDAO {
    private List<Group> groups = new ArrayList<>();
    private List<StudentToGroup> databaseStudentToGroups = new ArrayList<>();

    public void createGroupId(Group group) {
        if (groups.isEmpty()) {
            group.setId(1);
        } else {
            group.setId(groups.get(groups.size() - 1).getId() + 1);
        }
        groups.add(group);
    }

    public void addStudentToGroup(int groupId, int studentId) {
        StudentToGroup studentToGroup = new StudentToGroup();
        createGroupToStudentId(studentToGroup);
        studentToGroup.setGroupId(groupId);
        studentToGroup.setStudentId(studentId);
        databaseStudentToGroups.add(studentToGroup);
    }

    public void removeStudentOnGroup(int idStudent) {
        groups = groups.stream()
                .filter(student -> student.getId() != idStudent)
                .collect(Collectors.toList());
    }

    public void createGroupToStudentId(StudentToGroup studentToGroup) {
        if (databaseStudentToGroups.isEmpty()) {
            studentToGroup.setId(1);
        } else {
            studentToGroup.setId(databaseStudentToGroups.get(databaseStudentToGroups.size() - 1).getId() + 1);
        }

    }

    public List<Group> getGroups() {
        return groups;
    }

    public List<StudentToGroup> getDatabaseStudentToGroups() {
        return databaseStudentToGroups;
    }
}
