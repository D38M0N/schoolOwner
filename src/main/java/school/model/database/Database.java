package school.model.database;

import school.model.baseModel.Group;
import school.model.baseModel.Student;

public class Database {
    private static Database instance;
    private DatabaseSubjectDAO subjectDAO;
    private DatabaseGradeDAO gradeDAO;
    private DatabaseStudentDAO studentDAO;
    private DatabaseGroupDAO groupDAO;

    public Database() {
    }

    public static Database getInstance() {
        if (instance == null) {
            instance = new Database();
            instance.subjectDAO = new DatabaseSubjectDAO();
            instance.gradeDAO = new DatabaseGradeDAO();
            instance.studentDAO = new DatabaseStudentDAO();
            instance.groupDAO = new DatabaseGroupDAO();
        }
        return instance;
    }

    public void addStudent(Student student) {

        instance.studentDAO.addStudent(student);
    }

    public void addStudentToGroup(int groupId, int studentId) {
        instance.groupDAO.addStudentToGroup(groupId, studentId);
    }

    public void createGroupId(Group group) {
        instance.getGroupDAO().createGroupId(group);
    }

    public DatabaseSubjectDAO getSubjectDAO() {
        return subjectDAO;
    }

    public DatabaseGradeDAO getGradeDAO() {
        return gradeDAO;
    }

    public DatabaseStudentDAO getStudentDAO() {
        return studentDAO;
    }

    public DatabaseGroupDAO getGroupDAO() {
        return groupDAO;
    }

    public void calculateAverageForSubject(int studentId) {
        subjectDAO.getSubjectsForStudent(studentId)
                .forEach(subjectToStudent -> gradeDAO.calculateAverage(subjectToStudent));
    }


    public void deleteStudent(int idStudent) {
        gradeDAO.removeGradeStudentOnId(idStudent);
        subjectDAO.removeSubjectOnId(idStudent);
        studentDAO.removeStudentOnId(idStudent);
        groupDAO.removeStudentOnGroup(idStudent);
    }

//    public double calculateAverage
}
