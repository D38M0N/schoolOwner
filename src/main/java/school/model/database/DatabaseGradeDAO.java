package school.model.database;

import school.model.baseModel.Grade;
import school.model.baseModel.SubjectToStudent;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class DatabaseGradeDAO implements GradeDAO {

    private List<Grade> databaseGrades = new ArrayList<>();

    public void createGradeId(Grade grade) {

        if (databaseGrades.isEmpty()) {
            grade.setId(1);
        } else {
            grade.setId(databaseGrades.get(databaseGrades.size() - 1).getId() + 1);
        }
    }

    public void addGrade(double gradeToAdd, SubjectToStudent subjectToStudent) {
        Grade grade = new Grade();

        createGradeId(grade);
        grade.setSubjectWithStudentId(subjectToStudent.getId());
        grade.setGrade(gradeToAdd);
        databaseGrades.add(grade);
    }

    public void removeGradeStudentOnId(int id) {
        databaseGrades = databaseGrades.stream()
                .filter(student -> student.getId() != id)
                .collect(Collectors.toList());
    }

    public void calculateAverage(SubjectToStudent subjectToStudent) {
        double sumOfGrades = databaseGrades.stream()
                .filter(grade -> grade.getSubjectWithStudentId() == subjectToStudent.getId())
                .mapToDouble(Grade::getGrade).reduce(0, (sum, rate) -> sum + rate);

        long count = databaseGrades.stream()
                .filter(grade -> grade.getSubjectWithStudentId() == subjectToStudent.getId())
                .count();

        subjectToStudent.setAverage(sumOfGrades / count);
    }

    public void improveGrade(int gradeId, double improvedGrade) {
        Optional<Grade> gradeToImprove = databaseGrades.stream()
                .filter(grade -> grade.getId() == gradeId)
                .findAny();
        if (gradeToImprove.isPresent() && gradeToImprove.get().getImprovedGrade() == -1) {
            gradeToImprove.ifPresent(grade -> grade.setImprovedGrade(improvedGrade));
        }
    }

    public List<Grade> getGrates() {
        return databaseGrades;
    }
}
