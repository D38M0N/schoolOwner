package school.model.database;

import school.model.baseModel.Subject;
import school.model.baseModel.SubjectToStudent;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class DatabaseSubjectDAO implements SubjectDAO {

    private List<Subject> subjects = new ArrayList<>();
    private List<SubjectToStudent> databaseSubjectsToStudents = new ArrayList<>();

    public List<Subject> getSubjects() {
        return subjects;
    }

    public List<SubjectToStudent> getDatabaseSubjectsToStudents() {
        return databaseSubjectsToStudents;
    }

    public void addSubject(Subject subject) {
        if (subjects.isEmpty()) {
            subject.setId(1);
        } else {
            subject.setId(subjects.get(subjects.size() - 1).getId() + 1);
        }
        subjects.add(subject);
    }

    public void removeSubjectOnId(int id) {
        subjects = subjects.stream()
                .filter(subject -> subject.getId() != id)
                .collect(Collectors.toList());
    }

    public void addStudentToSubject(int subjectId, int studentId) {
        SubjectToStudent subjectToStudent = new SubjectToStudent();
        createStudentWithSubjectId(subjectToStudent);
        subjectToStudent.setSubjectId(subjectId);
        subjectToStudent.setStudentId(studentId);
        databaseSubjectsToStudents.add(subjectToStudent);
    }

    public void createStudentWithSubjectId(SubjectToStudent subjectToStudent) {
        if (databaseSubjectsToStudents.isEmpty()) {
            subjectToStudent.setId(1);
        } else {
            subjectToStudent.setId(databaseSubjectsToStudents.get(databaseSubjectsToStudents.size() - 1).getId() + 1);
        }
         }

// BIN BIN BIN BIN BIN BIN BIN BIN BIN BIN BIN BIN BIN BIN BIN BIN BIN BIN BIN BIN BIN BIN BIN BIN BIN BIN BIN BIN BIN
//    public void addGradeToStudent(int studentId, int subjectId, double gradeToAdd){
//        GradeDAO gradeDAO = new DatabaseGradeDAO(); //To be corrected, should return searchedSubjectToStudent to Controller and then school.controller triggers add grade method
//
//        Optional<SubjectToStudent> searchedSubjectToStudent = databaseSubjectsToStudents.stream()
//                .filter(subjectToStudent -> subjectToStudent.getStudentId() == studentId)
//                .filter(subjectToStudent -> subjectToStudent.getSubjectId() == subjectId)
//                .findAny();
//
//        searchedSubjectToStudent.ifPresent(subjectToStudent -> gradeDAO.addGrade(gradeToAdd, subjectToStudent));
//    }

    public List<SubjectToStudent> getSubjectsForStudent(int studentId) {
        return databaseSubjectsToStudents.stream()
                .filter(subjectToStudent -> subjectToStudent.getStudentId() == studentId)
                .collect(Collectors.toList());


// BIN BIN BIN BIN BIN BIN BIN BIN BIN BIN BIN BIN BIN BIN BIN BIN BIN BIN BIN BIN BIN BIN BIN BIN BIN BIN BIN BIN BIN
//        tempList.forEach(subjectToStudent -> {
//            subjectListener.getSubjects(subjects.stream()
//                    .filter(subject -> subject.getId() == subjectToStudent.getSubjectId())
//                    .collect(Collectors.toList()));
//        });


    }

    public SubjectToStudent getSubjectToStudent(int studentId, int subjectId){
        Optional<SubjectToStudent> searchedSubjectToStudent = databaseSubjectsToStudents.stream()
                .filter(subjectToStudent -> subjectToStudent.getStudentId() == studentId)
                .filter(subjectToStudent -> subjectToStudent.getSubjectId() == subjectId)
                .findAny();

        return searchedSubjectToStudent.orElse(null);
    }
}