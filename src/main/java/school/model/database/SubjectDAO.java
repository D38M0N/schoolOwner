package school.model.database;

import school.model.baseModel.Subject;
import school.model.baseModel.SubjectToStudent;

import java.util.List;

public interface SubjectDAO {
    void addSubject(Subject subject);

    void removeSubjectOnId(int id);

    void createStudentWithSubjectId(SubjectToStudent subjectToStudent);

    void addStudentToSubject(int subjectId, int studentId);

    List<Subject> getSubjects();

    List<SubjectToStudent> getDatabaseSubjectsToStudents();
}
