package menu.controller;

import menu.view.MenuMVC;
import scanner.view.ScannerMVC;
import school.view.SchoolMVC;

public class EndProgram implements MenuMVC.Controller.Menu {

    public void performActions(SchoolMVC.View schoolView) {
        schoolView.printEndProgramMessage();
    }
}
