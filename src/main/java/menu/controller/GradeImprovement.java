
package menu.controller;

import menu.view.MenuMVC;
import scanner.controller.ScannerController;
import scanner.view.ScannerMVC;
import school.model.database.Database;
import school.view.SchoolMVC;


public class GradeImprovement implements MenuMVC.Controller.Menu {
    private ScannerMVC.Controller scannerController;

    public GradeImprovement() {
        this.scannerController = new ScannerController();
    }

    @Override
    public void performActions(SchoolMVC.View schoolView) {

        schoolView.printAllStudents();
        int studentID = scannerController.nextInt();
        schoolView.printSubjectForStudent(studentID);

        schoolView.printAskForSubjectForStudentId();
        int subjectWithStudentID = scannerController.nextInt();  //scanner
        schoolView.printGradeForSubjectWithStudentToImprovement(subjectWithStudentID);

        schoolView.printAskForGradeId();
        int gradeID = scannerController.nextInt();

        schoolView.printAskForNewImprovementGrade();

        double improvedGrade = scannerController.nextDouble();   // scanner

        Database.getInstance().getGradeDAO().improveGrade(gradeID, improvedGrade);

        schoolView.printGradeForSubjectWithStudentToImprovement(subjectWithStudentID);
    }


}

