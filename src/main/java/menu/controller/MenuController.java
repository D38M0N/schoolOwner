package menu.controller;

import menu.model.*;
import menu.view.MenuMVC;

public class MenuController implements MenuMVC.Controller {
    private MenuOptions menuOptions;
    private MenuMVC.View menuView;

    public MenuController() {
        menuOptions = new MenuOptions();
    }

//    @Override
//    public void attach(MenuMVC.View menuView) {
//        this.menuView = menuView;
//    }

    public void loadMenu() {
        menuOptions.addMenuItem(0, new EndProgram());
        menuOptions.addMenuItem(1, new CreateGroup());
        menuOptions.addMenuItem(2, new CreateStudent());
        menuOptions.addMenuItem(3, new AddStudentToGroup());
        menuOptions.addMenuItem(4, new SubjectAddition());
        menuOptions.addMenuItem(5, new GradeAddition());
        menuOptions.addMenuItem(6, new GradeImprovement());
        menuOptions.addMenuItem(7, new SchoolYearEnd());
        menuOptions.addMenuItem(8, new ShowStudents());
    }

    public MenuOptions getMenuOptions() {
        return menuOptions;
    }

}
