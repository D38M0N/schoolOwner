package menu.controller;

import menu.view.MenuMVC;
import scanner.controller.ScannerController;
import scanner.view.ScannerMVC;
import school.model.baseModel.Student;
import school.model.database.Database;
import school.view.SchoolMVC;

public class CreateStudent implements MenuMVC.Controller.Menu {
    private ScannerMVC.Controller scannerController;

    public CreateStudent() {
        this.scannerController = new ScannerController();
    }

    @Override
    public void performActions(SchoolMVC.View schoolView) {


        schoolView.printAskForNewStudentsName();
        String studentsName = scannerController.nextLine();
        schoolView.printAskForNewStudentsSurname();
        String studentsSurname = scannerController.nextLine();
        Database.getInstance().addStudent(new Student(studentsName,studentsSurname));
        schoolView.printAllStudents();
    }
}
