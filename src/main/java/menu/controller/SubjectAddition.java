package menu.controller;

import menu.view.MenuMVC;
import scanner.controller.ScannerController;
import scanner.view.ScannerMVC;
import school.model.baseModel.Subject;
import school.model.database.Database;
import school.view.SchoolMVC;

public class SubjectAddition implements MenuMVC.Controller.Menu {
    private ScannerMVC.Controller scannerController;

    public SubjectAddition() {
        this.scannerController = new ScannerController();
    }

    @Override
    public void performActions(SchoolMVC.View schoolView) {
        schoolView.printAllSubjects();
        schoolView.printAskNameSubject();

        Database.getInstance().getSubjectDAO()
                .addSubject(new Subject(scannerController.nextLine()));

        schoolView.printAllSubjects();
        schoolView.printAllStudents();
        int subjectId;
        int studentId;
        schoolView.printAskForSubjectId();
        subjectId = scannerController.nextInt();
        schoolView.printAskForStudentId();
        studentId = scannerController.nextInt();
        Database.getInstance().getSubjectDAO().addStudentToSubject(subjectId, studentId);
    }
}
