package menu.controller;

import menu.view.MenuMVC;
import scanner.controller.ScannerController;
import scanner.view.ScannerMVC;
import school.model.database.Database;
import school.view.SchoolMVC;

public class AddStudentToGroup implements MenuMVC.Controller.Menu  {
private ScannerMVC.Controller scannerController;

    public AddStudentToGroup() {
        this.scannerController = new ScannerController();
    }

    @Override
    public void performActions(SchoolMVC.View schoolView) {

        int groupId;
        int studentId;

       schoolView.printAllGroups();
        schoolView.printAllStudents();

        schoolView.printAskForGroupId();
        groupId = scannerController.nextInt();

        schoolView.printAskForStudentId();
        studentId = scannerController.nextInt();

        Database.getInstance().addStudentToGroup(groupId,studentId);
        schoolView.printStudentInGroup(groupId);
    }
}
