package menu.controller;

import menu.view.MenuMVC;
import scanner.controller.ScannerController;
import scanner.view.ScannerMVC;
import school.model.baseModel.Group;
import school.model.database.Database;
import school.view.SchoolMVC;

public class CreateGroup implements MenuMVC.Controller.Menu {
    public CreateGroup() {
        this.scannerController = new ScannerController();
    }

    private ScannerMVC.Controller scannerController;

    @Override
    public void performActions(SchoolMVC.View schoolView) {

        schoolView.printAskForNewGroupMessage();
        Database.getInstance().getGroupDAO().createGroupId(new Group(scannerController.nextLine()));
        schoolView.printAllGroups();
    }
}
