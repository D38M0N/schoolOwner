package menu.controller;

import menu.view.MenuMVC;
import scanner.controller.ScannerController;
import scanner.view.ScannerMVC;
import school.model.baseModel.SubjectToStudent;
import school.model.database.Database;
import school.view.SchoolMVC;

public class GradeAddition implements MenuMVC.Controller.Menu {
    private ScannerMVC.Controller scannerController;

    public GradeAddition() {
        this.scannerController = new ScannerController();
    }


    @Override
    public void performActions(SchoolMVC.View schoolView) {
        schoolView.printAllStudents();
        schoolView.printAskForStudentId();

        int studentID = scannerController.nextInt();
        schoolView.printSubjectForStudent(studentID);

        schoolView.printAskForSubjectForStudentId();
        int subjectWithStudentID = scannerController.nextInt();  //scanner
        SubjectToStudent subjectToStudentAdd = Database.getInstance()
                .getSubjectDAO()
                .getSubjectToStudent(studentID, subjectWithStudentID);
        schoolView.printAskForNewGrade();
        double addGrade = scannerController.nextDouble();
        Database.getInstance().getGradeDAO().addGrade(addGrade, subjectToStudentAdd);
    }
}