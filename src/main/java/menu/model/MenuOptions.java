package menu.model;

import menu.view.MenuMVC;

import java.util.ArrayList;
import java.util.List;

public class MenuOptions {

    private List<MenuMVC.Controller.Menu> menuList;

    public MenuOptions() {
        this.menuList = new ArrayList<>();
    }

    public void addMenuItem(int index, MenuMVC.Controller.Menu menuItem) {
        this.menuList.add(index, menuItem);

    }

    public MenuMVC.Controller.Menu getMenuItem(int index) {
        return menuList.get(index);
    }
}
