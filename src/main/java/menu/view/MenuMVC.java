package menu.view;

import menu.model.MenuOptions;
import scanner.view.ScannerMVC;
import school.view.SchoolMVC;

public interface MenuMVC {

    interface View {
        void printMenuOptions();
        void printEndProgramMessage();
    }

    interface Controller {

        interface Menu {
            void performActions(SchoolMVC.View schoolView);
        }

        //        void attach(View menuView);
        void loadMenu();

        MenuOptions getMenuOptions();
    }
}
