package menu.view;

import console.controller.ConsoleController;
import console.view.ConsoleMVC;

public class MenuView implements MenuMVC.View {

    private ConsoleMVC.Controller consoleController;

    public MenuView() {
        consoleController = new ConsoleController();
    }
// Marcin
    public void printMenuOptions() {
        consoleController.printLine("0. End Program");
        consoleController.printLine("1. Create New Students Group");
        consoleController.printLine("2. Create New Student");
        consoleController.printLine("3. Add Student to the Group");
        consoleController.printLine("4. Add Subject to Student");
        consoleController.printLine("5. Add Grade to Student");
        consoleController.printLine("6. Improve Grade");
        consoleController.printLine("8. Present student");
    }

    public void printEndProgramMessage(){
        consoleController.printLine("Goodbye");
    }

}


