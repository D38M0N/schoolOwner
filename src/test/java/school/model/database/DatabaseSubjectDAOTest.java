package school.model.database;

import org.junit.Assert;
import org.junit.Test;
import school.model.baseModel.Student;
import school.model.baseModel.Subject;

import static org.junit.Assert.*;

public class DatabaseSubjectDAOTest {
    private DatabaseSubjectDAO databaseSubjectDAO = new DatabaseSubjectDAO();
    private Subject subject = new Subject("Javaa");
    private Subject subject2 = new Subject("Java");
    private Student student2 = new Student("P", "B");

    @Test
    public void addSubject() {
        databaseSubjectDAO.addSubject(subject);
        databaseSubjectDAO.addSubject(subject2);
        Assert.assertEquals(1, databaseSubjectDAO.getSubjects().get(0).getId());
        Assert.assertEquals(2, databaseSubjectDAO.getSubjects().get(1).getId());

    }

    @Test
    public void removeSubjectOnId() {
        databaseSubjectDAO.addSubject(subject);
        databaseSubjectDAO.addSubject(subject2);
        databaseSubjectDAO.removeSubjectOnId(1);
        databaseSubjectDAO.removeSubjectOnId(2);
        Assert.assertTrue(databaseSubjectDAO.getSubjects().isEmpty());

    }

    @Test
    public void addStudentToSubject() {
        databaseSubjectDAO.addStudentToSubject(subject.getId(),student2.getId());
        databaseSubjectDAO.addStudentToSubject(subject2.getId(),student2.getId());
        Assert.assertTrue(databaseSubjectDAO.getDatabaseSubjectsToStudents().size()==2);

    }

    @Test
    public void createStudentWithSubjectId() {
        databaseSubjectDAO.addStudentToSubject(subject.getId(),student2.getId());
        databaseSubjectDAO.addStudentToSubject(subject2.getId(),student2.getId());
        Assert.assertTrue(databaseSubjectDAO.getDatabaseSubjectsToStudents().get(0).getId()==1);
        Assert.assertTrue(databaseSubjectDAO.getDatabaseSubjectsToStudents().get(1).getId()==2);
    }

}