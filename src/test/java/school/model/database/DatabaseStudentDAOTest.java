package school.model.database;

import org.junit.Assert;
import org.junit.Test;
import school.model.baseModel.Student;

public class DatabaseStudentDAOTest {
    private Student student1 = new Student("M", "Ch");
    private Student student2 = new Student("P", "B");
    private Student student3 = new Student("D", "C");
    private StudentDAO testedStudentDAO = Database.getInstance().getStudentDAO();

    @Test
    public void addOneStudentTest() {
        testedStudentDAO.addStudent(student1);
        Assert.assertTrue(testedStudentDAO.getStudents().contains(student1));
    }

    @Test
    public void addMoreStudentsTest() {
        testedStudentDAO.addStudent(student1);
        testedStudentDAO.addStudent(student2);
        Assert.assertTrue(testedStudentDAO.getStudents().contains(student1));
        Assert.assertTrue(testedStudentDAO.getStudents().contains(student2));
    }

    @Test
    public void removeStudentOnIdTest(){
        testedStudentDAO.addStudent(student1);
        testedStudentDAO.addStudent(student2);
        testedStudentDAO.addStudent(student3);

        testedStudentDAO.removeStudentOnId(student2.getId());
        Assert.assertFalse(testedStudentDAO.getStudents().contains(student2));
        Assert.assertTrue(testedStudentDAO.getStudents().contains(student1));
        Assert.assertTrue(testedStudentDAO.getStudents().contains(student3));
    }

}