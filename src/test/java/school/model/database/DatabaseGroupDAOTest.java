package school.model.database;

import org.junit.Assert;
import org.junit.Test;
import school.model.baseModel.Group;
import school.model.baseModel.Student;
import school.model.baseModel.StudentToGroup;

import java.util.Optional;

public class DatabaseGroupDAOTest {
    GroupDAO testedGroupDAO = new DatabaseGroupDAO();
    Group group1 = new Group("I");
    Group group2 = new Group("II");
    Student student1 = new Student("M", "C");
    Student student2 = new Student("P", "B");

    @Test
    public void addOneGroupTest() {
        testedGroupDAO.createGroupId(group1);
        testedGroupDAO.createGroupId(group2);
        Assert.assertTrue(testedGroupDAO.getGroups().contains(group1));
    }

    @Test
    public void addMoreGroupsTest() {
        testedGroupDAO.createGroupId(group1);
        testedGroupDAO.createGroupId(group2);

        Assert.assertTrue(testedGroupDAO.getGroups().contains(group1));
        Assert.assertTrue(testedGroupDAO.getGroups().contains(group2));
    }

    @Test
    public void addStudentsToGroupTest() {
        testedGroupDAO.createGroupId(group1);
        testedGroupDAO.addStudentToGroup(group1.getId(), student1.getId());
        testedGroupDAO.addStudentToGroup(group1.getId(), student2.getId());

        Optional<StudentToGroup> studentToGroupStudent1 = testedGroupDAO.getDatabaseStudentToGroups().stream()
                .filter(group -> group.getStudentId() == student1.getId()&& group.getId() == group1.getId())
                .findAny();

        Optional<StudentToGroup> studentToGroupStudent2 = testedGroupDAO.getDatabaseStudentToGroups().stream()
                .filter(group -> group.getStudentId() == student1.getId() && group.getId() == group1.getId())
                .findAny();

        if (studentToGroupStudent1.isPresent()) {
            Assert.assertEquals(student1.getId(), studentToGroupStudent1.get().getStudentId());
            Assert.assertEquals(student2.getId(), studentToGroupStudent2.get().getStudentId());
        }
    }

}