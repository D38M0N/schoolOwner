package school.model.database;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import school.model.baseModel.*;

import static org.junit.Assert.*;

public class DatabaseGradeDAOTest {
    private GradeDAO testGradeDAO = new DatabaseGradeDAO();
    private SubjectToStudent subjectToStudent = new SubjectToStudent();
    private Student student = new Student("Pawel", "B");
    private Subject subject = new Subject("ENG");


    @Test
    public void createGradeId() {
        testGradeDAO.addGrade(5, subjectToStudent);
        testGradeDAO.addGrade(5, subjectToStudent);
        Assert.assertEquals(1, testGradeDAO.getGrates().get(0).getId());
        Assert.assertEquals(2, testGradeDAO.getGrates().get(1).getId());


    }

    @Test
    public void removeGradeStudentOnId() {
        testGradeDAO.addGrade(5, subjectToStudent);
        testGradeDAO.addGrade(5, subjectToStudent);
        testGradeDAO.removeGradeStudentOnId(1);
        testGradeDAO.removeGradeStudentOnId(2);
        Assert.assertTrue(testGradeDAO.getGrates().isEmpty());
    }

    @Test
    public void improveGrade() {
        testGradeDAO.addGrade(5, subjectToStudent);
        testGradeDAO.improveGrade(1, 3.0);
        Assert.assertTrue(5.0 == testGradeDAO.getGrates().get(0).getGrade());
        Assert.assertTrue(3.0 == testGradeDAO.getGrates().get(0).getImprovedGrade());
    }

    @Test
    public void calculateAverage() {

        subject.setId(1);
        student.setId(1);
        subjectToStudent.setId(1);

        subjectToStudent.setStudentId(student.getId());
        subjectToStudent.setSubjectId(subject.getId());
        testGradeDAO.addGrade(3.0, subjectToStudent);
        testGradeDAO.addGrade(4.0, subjectToStudent);

        testGradeDAO.calculateAverage(subjectToStudent);

        Assert.assertTrue(subjectToStudent.getAverage() == 3.5);
    }
}