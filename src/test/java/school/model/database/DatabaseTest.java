package school.model.database;

import org.junit.Assert;
import org.junit.Test;
import school.model.baseModel.*;


public class DatabaseTest {
    private Database database = new Database().getInstance();
    private Subject subjectJ = new Subject("java");
    private Student student = new Student("Pawel", "Buczek");
    private Student student2 = new Student("Pawel", "Buczek");
    private Group group = new Group("II");


    @Test
    public void addStudent() {
        database.addStudent(student);
        database.getStudentDAO();
        Assert.assertTrue(database.getStudentDAO().getStudents().contains(student));

    }

    @Test
    public void addStudentToGroup() {
        database.addStudentToGroup(group.getId(), student.getId());
        database.addStudentToGroup(group.getId(), student2.getId());
        Assert.assertTrue(database.getGroupDAO().getDatabaseStudentToGroups().size() == 2);

    }

    @Test
    public void createGroupId() {
        database.createGroupId(group);
        Assert.assertTrue(database.getGroupDAO().getGroups().contains(group));
        Assert.assertTrue(database.getGroupDAO().getGroups().size()==1);
    }

    @Test
    public void calculateAverageForSubject() {
        database.getInstance().getSubjectDAO().addSubject(subjectJ);
        student.setId(1);
        int subjectID = database.getSubjectDAO().getSubjects().get(0).getId();
       database.getSubjectDAO().addStudentToSubject(subjectID,student.getId());
        SubjectToStudent subjectToStudent = database.getSubjectDAO().getSubjectToStudent(subjectID,student.getId());
        database.getGradeDAO().addGrade(4,subjectToStudent);
        database.getGradeDAO().addGrade(3,subjectToStudent);
        database.calculateAverageForSubject(subjectID);
       Assert.assertTrue(3.5==database.getSubjectDAO().getSubjectToStudent(student.getId(),subjectID).getAverage());

    }


    @Test
    public void deleteStudent() {
        database.addStudent(student);
        database.deleteStudent(1);

        Assert.assertTrue(database.getSubjectDAO().getSubjects().isEmpty());
        Assert.assertTrue(database.getGradeDAO().getGrates().isEmpty());
        Assert.assertTrue(database.getStudentDAO().getStudents().isEmpty());
        Assert.assertTrue(database.getGroupDAO().getGroups().isEmpty());
    }
}