package school.view;

import org.junit.Test;
import school.model.baseModel.Student;
import school.model.baseModel.Subject;
import school.model.baseModel.SubjectToStudent;
import school.model.database.Database;

import static org.junit.Assert.*;

public class SchoolViewTest {
    private Database database = new Database().getInstance();
    SchoolView schoolView = new SchoolView();
    private Student student = new Student("Pawel", "B");
    private Subject subject = new Subject("ENG");

    @Test
    public void printSubjectForStudent() {
        database.addStudent(student);
        database.getSubjectDAO().addSubject(subject);
        database.getSubjectDAO().addStudentToSubject(subject.getId(),student.getId());
        schoolView.printSubjectForStudent(student.getId());
    }
}